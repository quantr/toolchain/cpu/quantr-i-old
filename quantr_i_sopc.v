`include "defines.v"

module quantr_i_sopc(input wire clk, input wire rst);
    wire[`InstAddrBus]	inst_addr;
    wire[`InstBus]		inst;
    wire				rom_ce;
    
    quantr_i quantr_i_cpu(
        .clk(clk),
        .rst(rst),
        
        .rom_addr_o(inst_addr),
        .rom_data_i(inst),
        .rom_ce_o(rom_ce)
    );
    
    inst_rom inst_rom0(
        .addr(inst_addr),
        .inst(inst),
        .ce(rom_ce)
    );
endmodule

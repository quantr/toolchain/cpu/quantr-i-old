#include "Vquantr_i_sopc.h"
#include "verilated.h"
#include <verilated_vcd_c.h>

vluint64_t main_time = 0;
VerilatedVcdC *tfp;

double sc_time_stamp() { return main_time; }

int main(int argc, char **argv, char **env)
{
	Verilated::commandArgs(argc, argv);
	Vquantr_i_sopc *counter = new Vquantr_i_sopc;

	Verilated::traceEverOn(true);
	tfp = new VerilatedVcdC;
	counter->trace(tfp, 99);
	tfp->open("output.vcd");

	//while (!Verilated::gotFinish())
	for (main_time = 0; main_time < 90; main_time++)
	{
		// printf("main_time=%ld\n",main_time);
		/*
		if ((main_time % 10) == 1)
		{
			counter->clk = 1;
		}
		if ((main_time % 10) == 6)
		{
			counter->clk = 0;
		}
		*/

		counter->clk = !counter->clk;
		counter->eval();
		tfp->dump(main_time);
		//main_time++;
	}
	counter->final();
	delete counter;

	tfp->close();
	exit(0);
}

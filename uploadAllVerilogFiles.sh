#!/bin/bash

rm -fr temp
ls *.v|while read a; do
	echo $a;
	if [ -s "temp" ]; then
		echo "," >>temp
	fi
	java -jar quantr-verilog-tool-*.jar -i $a >> temp
done
echo "[$(cat temp)]" > temp
cat temp | curl -i -X POST https://www.quantr.foundation/wp-json/wordpress-vcd/uploadInout/peter@quantr.hk/1234/Quantr_i -d @-
#rm -fr temp

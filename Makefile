all:
	verilator --trace --cc quantr_i_sopc.v --exe sim_main.cpp # -debug --assert
	make OPT_FAST="-O2 -fno-stack-protector" -j -C obj_dir -f Vquantr_i_sopc.mk Vquantr_i_sopc

run:
	obj_dir/Vquantr_i_sopc +verilator+debug

clean:
	rm -fr obj_dir
	rm output.vcd
	
yosys:
	cat sys.yo | yosys
	cp ~/.yosys_show.dot yosys_show.dot
	dot -Tsvg yosys_show.dot -o output.svg
	curl -i -X POST -H "Content-Type: multipart/form-data" -F "file1=@output.svg" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadYosys/peter@quantr.hk/1234/Quantr_i
	curl -i -X POST -H "Content-Type: multipart/form-data" -F "file1=@output.vcd" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadVcd/peter@quantr.hk/1234/Quantr_i
	curl -i -X POST https://www.quantr.foundation/wp-json/wordpress-vcd/uploadScopeSequence/peter@quantr.hk/1234/Quantr_i/pc_reg0,if_id0,id0,regfile1,id_ex0,ex0,ex_mem0,mem0,mem_wb0
	#./uploadAllVerilogFiles.sh
	ls *.v|xargs java -jar quantr-verilog-tool-1.0.jar -i | curl -i -X POST https://www.quantr.foundation/wp-json/wordpress-vcd/uploadInout/peter@quantr.hk/1234/Quantr_i -d @-

	rm output.svg
